Ext.define("CustomApp", {
    extend: "Rally.app.App",
    componentCls: "app",
    // Let's configure
    //
    pInitialPageLayout: "grid",
    pInitialColumns: ["Name", "ScheduleState", "State", "Iteration"],
    pInitialArtefacts: ["ArtifactSearch", "Release", "Project", "ModelType"],
    pInitialModels: ["defect", "userstory", "testcase"],
    itemId: 'thisApp',
    //
    items: [{
        xtype: 'container',
        itemId: 'north',
        region: 'north',
        minHeight: 200
    }, {
        xtype: 'container',
        itemId: 'center',
        region: 'center',
        listeners: {
            afterrender: function() {
                gApp = this.up('#thisApp');
                gAppNorth = gApp.down('#north');
                gAppCentre = gApp.down('#center');
                gApp._treeStoreBuilder();
            },
        },
    }],
    //
    launch: function() {},

    _treeStoreBuilder: function() {
        Ext.create("Rally.data.wsapi.TreeStoreBuilder").build({
            models: this.pInitialModels,
            autoLoad: !0,
            enableHierarchy: !0
        }).then({
            success: function(el) {
                this._onStoreBuilt(el);
            },
            scope: this
        });
    },
    _onStoreBuilt: function(store) {
        console.log('App ID ', gApp);
        console.log('App ID North ', gAppNorth);
        console.log('App ID Centre ', gAppCentre);
        var modelNames = this.pInitialModels,
            context = this.getContext();
        gAppCentre.add({
            xtype: "rallygridboard",
            context: context,
            modelNames: modelNames,
            toggleState: this.pInitialPageLayout,
            stateful: !1,
            plugins: ["rallygridboardaddnew", {
                ptype: "rallygridboardinlinefiltercontrol",
                inlineFilterButtonConfig: {
                    stateful: !0,
                    stateId: context.getScopedStateId("filters"),
                    modelNames: modelNames,
                    inlineFilterPanelConfig: {
                        quickFilterPanelConfig: {
                            defaultFields: this.pInitialArtefacts
                        }
                    }
                }
            }, {
                ptype: "rallygridboardfieldpicker",
                headerPosition: "left",
                modelNames: modelNames,
                stateful: !0,
                stateId: context.getScopedStateId("columns-example")
            }, {
                ptype: "rallygridboardactionsmenu",
                menuItems: [{
                    text: "Export...",
                    handler: function() {
                        window.location = Rally.ui.gridboard.Export.buildCsvExportUrl(this.down("rallygridboard").getGridOrBoard());
                    },
                    scope: this
                }],
                buttonConfig: {
                    iconCls: "icon-export"
                }
            }, "rallygridboardtoggleable"],
            cardBoardConfig: {
                attribute: "ScheduleState"
            },
            gridConfig: {
                store: store,
                columnCfgs: this.pInitialColumns,
            },
            height: this.getHeight()
        });
    }
});