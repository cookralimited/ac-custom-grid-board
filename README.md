Agile Central: Custom List
=========================

## Overview  

[Forked from Rally App Catalogue](https://github.com/RallyApps/app-catalog)  

## License  

Agile Central Custom List is released under the MIT license.  
See the file [LICENSE](./LICENSE) for the full text.  
Maintaned by:  
R Cook  
[Cookra Limited](https://www.agilerick.guru?gitHub="customList")  

##Documentation for SDK  

You can find the documentation on our help [site.](https://help.rallydev.com/apps/2.1/doc/)
